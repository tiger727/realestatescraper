var environment = {
    screen_width: 1920,
    screen_height: 1080,
    headless: true,
    timeout: 3000000
}

module.exports = environment;