var selectors = {
    SEARCH_BUTTON: 'button.rui-search-button',
    SEARCH_INPUT: '#where',
    RESULT_LIST:'#searchResultsTbl article.resultBody',
    RESULT_PROPERTY_AGENT_IMAGE: 'img.agent-photo',
    RESULT_PROPERTY_ADDRESS_LINK : '.vcard a'
}

module.exports = selectors;