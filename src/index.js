const puppeteer = require('puppeteer');
const environment = require('./environment')
const selectors = require('./selector')


async function start_scraper (){
	const browser = await puppeteer.launch({headless: false,defaultViewport:{width: environment.screen_width, height: environment.screen_height}});
	const page = await browser.newPage();
	await page.goto('https://www.realestate.com.au/buy/in-west+pennant+hills,+nsw+2125/list-1', {timeout: environment.timeout});


	// await page.click(selectors.SEARCH_INPUT);
	// await page.keyboard.type("West Pennant Hills");

  	// await page.click(selectors.SEARCH_BUTTON);
	// await page.waitForNavigation({timeout: environment.timeout});

	let results = await page.evaluate((sel, image_sel, link_sel) => {
		let items =  document.querySelectorAll(sel);

		let result = [];

		for(i = 0; i < items.length; i ++ )
		{
			let item = items[i];

			let agentInfo = item.querySelector(image_sel).alt;
			let address = item.querySelector(link_sel).innerHTML;

			result.push({
				agentInfo:agentInfo,
				address:address
			})
		}

		return result;
	}, selectors.RESULT_LIST, selectors.RESULT_PROPERTY_AGENT_IMAGE,selectors.RESULT_PROPERTY_ADDRESS_LINK)

	console.log(results);
}

start_scraper();
